package com.exadel.gs.ex.model;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Andrey Frunt
 */
@SpaceClass
public class Item {
    private Long id;

    private String name;

    private String description;

    private String categoryId;

    private String imageUrl;

    private Integer price;

    private Integer count;

    @SpaceId(autoGenerate = false)
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @NotEmpty(message="validation.category.name.NotEmpty.message")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @SpaceRouting
    @NotEmpty(message="validation.item.categoryId.NotEmpty.message")
    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @NotEmpty(message="validation.item.imageUrl.NotEmpty.message")
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    //@NotBlank(message="validation.item.price.NotEmpty.message")
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
