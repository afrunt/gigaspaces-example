package com.exadel.gs.ex.model;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Andrey Frunt
 */
@SpaceClass
public class Category {
    private String id;

    private String name;

    private String description;

    @SpaceId(autoGenerate = false)
    @NotEmpty(message="validation.category.id.NotEmpty.message")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NotEmpty(message="validation.category.name.NotEmpty.message")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
