package com.exadel.gs.ex.web.controller;

import com.exadel.gs.ex.model.Item;
import com.exadel.gs.ex.service.CategoryService;
import com.exadel.gs.ex.service.ItemService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/shop")
public class ShopController {
    private final Log log = LogFactory.getLog(ShopController.class);

    @Resource
    private CategoryService categoryService;

    @Resource
    private ItemService itemService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String show(Model uiModel) {
        uiModel.addAttribute("items", itemService.getItemsByCategoryId("0"));
        uiModel.addAttribute("categories", categoryService.findAll());
        uiModel.addAttribute("currentPlace", "shop");
        return "shop/show";
    }

    @RequestMapping(value = {"/category/{categoryId}", "/category/{categoryId}/"}, method = RequestMethod.GET)
    public String showCategory(@PathVariable("categoryId") String categoryId, Model uiModel) {
        return showCategoryPage(categoryId, 0, uiModel);
    }

    @RequestMapping(value = {"/category/{categoryId}/page/{pageNo}", "/category/{categoryId}/page/{pageNo}/"}, method = RequestMethod.GET)
    public String showCategoryPage(@PathVariable("categoryId") String categoryId, @PathVariable("pageNo") Integer pageNo, Model uiModel) {
        Item template = new Item();
        template.setCategoryId(categoryId);
        int total = itemService.getCountByTemplate(template);
        int perPage = 6;
        int pageCount = itemService.calculatePageCount(total,perPage);

        List<Item> items = itemService.getItemsByCategoryId(categoryId, pageNo, perPage);

        uiModel.addAttribute("perPage", perPage);
        uiModel.addAttribute("total", total);
        uiModel.addAttribute("pageNo", pageNo);
        uiModel.addAttribute("prevPageNo", pageNo == 0 ? 0 : pageNo - 1);
        uiModel.addAttribute("lastPage", pageNo == pageCount - 1);

        uiModel.addAttribute("items", items);
        uiModel.addAttribute("currentPlace", "shop");
        uiModel.addAttribute("currentCategory", categoryId);
        uiModel.addAttribute("categories",categoryService.findAll());

        return "shop/category/show";
    }

    private int lastPageCorrection(long itemsTotal, long itemsPerPage) {
        return ((itemsTotal % itemsPerPage > 0) ? 1 : 0);
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }
}
