package com.exadel.gs.ex.service.exception;

/**
 * @author Andrey Frunt
 */
public abstract class ServiceException extends Exception {
    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
