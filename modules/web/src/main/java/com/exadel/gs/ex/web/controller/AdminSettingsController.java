package com.exadel.gs.ex.web.controller;

import com.exadel.gs.ex.service.SampleDataService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/admin/settings")
public class AdminSettingsController {
    @Resource
    private SampleDataService sampleDataService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String showSettings(Model uiModel) {
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "settings");
        return "admin/settings/show";
    }

    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    public String generateSampleData() {
        sampleDataService.generate();
        return "redirect:/admin";
    }
}
