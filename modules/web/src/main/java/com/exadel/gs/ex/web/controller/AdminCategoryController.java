package com.exadel.gs.ex.web.controller;

import com.exadel.gs.ex.model.Category;
import com.exadel.gs.ex.service.CategoryService;
import com.exadel.gs.ex.service.exception.AlreadyExistsException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/admin/categories")
public class AdminCategoryController {
    private final Log log = LogFactory.getLog(AdminCategoryController.class);
    @Autowired
    private MessageSource messageSource;

    @Resource
    private CategoryService categoryService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String list(Model uiModel) {

        List<Category> all = categoryService.findAll();
        for (Category c : all) {
            log.info(c.getId() + " " + c.getId().hashCode());
        }
        uiModel.addAttribute("categories", all);
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "categories");
        return "admin/categories/list";
    }

    @RequestMapping(value = {"/delete/", "/delete"}, method = RequestMethod.POST)
    public String delete(Category category) {
        categoryService.delete(category);
        return "redirect:/admin/categories/";
    }

    @RequestMapping(value = {"/", ""}, params = "form", method = RequestMethod.GET)
    public String createForm(Model uiModel) {
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "categories");
        uiModel.addAttribute("category", new Category());
        uiModel.addAttribute("create", true);
        return "admin/categories/form";
    }

    @RequestMapping(value = {"/", ""}, params = "form", method = RequestMethod.POST)
    public String create(@Valid Category category, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Locale locale) {

        uiModel.addAttribute("create", true);
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "categories");

        if (bindingResult.hasErrors()) {

            List<ObjectError> errObjects = bindingResult.getAllErrors();
            List<String> allErrors = new ArrayList<String>(errObjects.size());

            for (ObjectError e : errObjects) {
                allErrors.add(messageSource.getMessage(e.getDefaultMessage(), e.getArguments(), locale));
            }

            uiModel.addAttribute("errors", allErrors);
            System.out.println(category.getName());
            return "admin/categories/form";
        } else {
            try {
                categoryService.create(category);
                uiModel.asMap().clear();
                return "redirect:/admin/categories";
            } catch (AlreadyExistsException e) {
                uiModel.addAttribute("errors", messageSource.getMessage("admin.category.exists", new String[]{category.getId()}, locale));
                return "admin/categories/form";
            }
        }
    }

    @RequestMapping(value = {"/{id}/", "/{id}"}, params = "form", method = RequestMethod.GET)
    public String updateForm(@PathVariable String id, Model uiModel) {
        Category category = categoryService.findById(id);

        System.out.println(category.getId());
        System.out.println(category.getName());
        if (category == null) {
            uiModel.asMap().clear();
            return "redirect:/admin/categories";
        }

        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "categories");
        uiModel.addAttribute("category", category);
        uiModel.addAttribute("create", false);
        return "admin/categories/form";
    }

    @RequestMapping(value = {"/{id}", "/{id}/"}, params = "form", method = RequestMethod.POST)
    public String update(@Valid Category category, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Locale locale) {

        uiModel.addAttribute("create", false);
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "categories");

        if (bindingResult.hasErrors()) {

            List<ObjectError> errObjects = bindingResult.getAllErrors();
            List<String> allErrors = new ArrayList<String>(errObjects.size());

            for (ObjectError e : errObjects) {
                allErrors.add(messageSource.getMessage(e.getDefaultMessage(), e.getArguments(), locale));
            }

            uiModel.addAttribute("category", category);
            uiModel.addAttribute("errors", allErrors);
            System.out.println(category.getName());
            return "admin/categories/form";
        } else {
            categoryService.update(category);
            uiModel.asMap().clear();
            return "redirect:/admin/categories";
        }
    }
}
