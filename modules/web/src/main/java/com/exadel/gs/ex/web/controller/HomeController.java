package com.exadel.gs.ex.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/home")
public class HomeController {

    @RequestMapping({"/",""})
    public String index(Model uiModel){
        uiModel.addAttribute("currentPlace", "home");
        return "home/show";
    }
}
