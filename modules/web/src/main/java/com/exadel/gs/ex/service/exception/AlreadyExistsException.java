package com.exadel.gs.ex.service.exception;

/**
 * @author Andrey Frunt
 */
public class AlreadyExistsException extends ServiceException {
    public AlreadyExistsException(String message) {
        super(message);
    }

    public AlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
