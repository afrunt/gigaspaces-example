package com.exadel.gs.ex.web.controller;

import com.exadel.gs.ex.model.Category;
import com.exadel.gs.ex.model.Item;
import com.exadel.gs.ex.service.CategoryService;
import com.exadel.gs.ex.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/admin/items")
public class AdminItemController {
    @Autowired
    private MessageSource messageSource;

    @Resource
    private CategoryService categoryService;

    @Resource
    private ItemService itemService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String list(Model uiModel, HttpServletRequest request) {
        List<Category> categories = categoryService.findAll();
        String categoryId = request.getParameter("categoryId");
        Category currentCategory = null;
        if (categoryId != null) {
            for (Category category : categories) {
                if (category.getId().equals(categoryId)) {
                    currentCategory = category;
                }
            }
        } else if (!categories.isEmpty()) {
            currentCategory = categories.get(0);
        }

        if (currentCategory == null) {
            /* TODO: 404 */
        }

        List<Item> items = itemService.getItemsByCategoryId(currentCategory.getId());

        uiModel.addAttribute("categories", categories);
        uiModel.addAttribute("currentCategory", currentCategory);
        uiModel.addAttribute("items", items);
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "items");
        return "admin/items/show";
    }

    @RequestMapping(value = {"/", ""}, params = "form", method = RequestMethod.GET)
    public String createForm(Model uiModel) {
        uiModel.addAttribute("categories", categoryService.findAll());
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "items");
        uiModel.addAttribute("item", new Item());
        uiModel.addAttribute("create", true);
        return "admin/items/form";
    }

    @RequestMapping(value = {"/", ""}, params = "form", method = RequestMethod.POST)
    public String create(@Valid Item item, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Locale locale) {

        uiModel.addAttribute("create", true);
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "items");

        if (bindingResult.hasErrors()) {
            List<ObjectError> errObjects = bindingResult.getAllErrors();
            List<String> allErrors = new ArrayList<String>(errObjects.size());

            for (ObjectError e : errObjects) {
                allErrors.add(messageSource.getMessage(e.getDefaultMessage(), e.getArguments(), locale));
            }

            uiModel.addAttribute("errors", allErrors);
            return "admin/items/form";
        } else {
            itemService.create(item);
            return "redirect:/admin/items";
        }
    }

    @RequestMapping(value = {"/{id}", "{id}/"}, params = "form", method = RequestMethod.GET)
    public String updateForm(@PathVariable Long id,Model uiModel) {
        Item item = itemService.findById(id);
        if(item == null) {
            return "redirect:/admin/items";
        }
        uiModel.addAttribute("categories", categoryService.findAll());
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "items");
        uiModel.addAttribute("item", item);
        uiModel.addAttribute("create", false);
        return "admin/items/form";
    }

    @RequestMapping(value = {"/{id}", "/{id}/"}, params = "form", method = RequestMethod.POST)
    public String update(@Valid Item item, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Locale locale) {

        uiModel.addAttribute("create", false);
        uiModel.addAttribute("currentPlace", "admin");
        uiModel.addAttribute("currentAdminPlace", "items");

        if (bindingResult.hasErrors()) {

            List<ObjectError> errObjects = bindingResult.getAllErrors();
            List<String> allErrors = new ArrayList<String>(errObjects.size());

            for (ObjectError e : errObjects) {
                allErrors.add(messageSource.getMessage(e.getDefaultMessage(), e.getArguments(), locale));
            }

            uiModel.addAttribute("item", item);
            uiModel.addAttribute("errors", allErrors);
            return "admin/items/form";
        } else {
            itemService.update(item);
            uiModel.asMap().clear();
            return "redirect:/admin/items";
        }
    }
}
