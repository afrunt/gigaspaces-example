package com.exadel.gs.ex.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/contacts")
public class ContactsController {

    @RequestMapping(value = {"/",""},method = RequestMethod.GET)
    public String show(Model uiModel){
        uiModel.addAttribute("currentPlace","contacts");
        return "contacts/show";
    }
}
