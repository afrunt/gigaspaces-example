package com.exadel.gs.ex.service;

import com.exadel.gs.ex.model.Category;
import com.exadel.gs.ex.model.Item;
import com.exadel.gs.ex.service.exception.AlreadyExistsException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openspaces.core.GigaSpace;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Andrey Frunt
 */
@Service
public class SampleDataService {
    private final Log log = LogFactory.getLog(SampleDataService.class);

    @Resource
    private GigaSpace gigaSpace;

    @Resource
    private CategoryService categoryService;

    @Resource
    private ItemService itemService;

    public void generate() {
        generateCategory("Laptops", "laptops", new String[]{"Samsung", "Lenovo", "Acer", "Apple"}, "notebook", 100000);
        generateCategory("Tablets", "tablets", new String[]{"Samsung", "Lenovo", "Acer", "Apple", "Motorola", "Toshiba"}, "tablet", 100000);
    }

    private void generateCategory(String categoryName, String categoryId, String[] brands, String itemName, int itemCount) {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        Category category = new Category();
        category.setId(categoryId);
        category.setName(categoryName);

        try {
            categoryService.create(category);
        } catch (AlreadyExistsException e) {
            log.error("Error", e);
        }

        log.info("Category " + categoryName + " created");
        Item[] items = new Item[itemCount];

        for (int i = 0; i < itemCount; i++) {
            Item item = new Item();
            int brandIndex = (int) (i * (System.currentTimeMillis() % 7)) % brands.length;
            String brand = brands[brandIndex];
            char modelLetter = alphabet.charAt(i % alphabet.length());
            int modelNum = (int) (i * (System.currentTimeMillis() % 7)) % 1000;
            item.setName(brand + " " + itemName + " " + modelLetter + modelNum);
            item.setImageUrl("z");
            item.setCategoryId(categoryId);
            item.setPrice(100);
            item.setCount(100);
            items[i] = item;
        }

        itemService.create(items);
        log.info(itemCount + " items created in category " + categoryName);
    }
}
