package com.exadel.gs.ex.service;

import com.exadel.gs.ex.idgenerator.client.IdGenerator;
import com.exadel.gs.ex.model.Item;
import com.j_spaces.core.client.SQLQuery;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openspaces.core.GigaSpace;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Service("itemService")
public class ItemService {
    private final Log log = LogFactory.getLog(ItemService.class);


    @Resource
    private IdGenerator idGenerator;

    @Resource
    private GigaSpace gigaSpace;

    private SQLQuery<Item> findByCategoryIdQuery = new SQLQuery<Item>(Item.class, "categoryId=? ORDER BY name");
    private SQLQuery<Item> findPageByCategoryIdQuery = new SQLQuery<Item>(Item.class, "ROWNUM>=? AND categoryId=? ORDER BY name");

    public List<Item> getItemsByCategoryId(String categoryId) {
        return Arrays.asList(gigaSpace.readMultiple(findByCategoryIdQuery.setParameter(1, categoryId)));
    }

    public List<Item> getItemsByCategoryId(String categoryId, int pageNo, int perPage) {
        Item countTemplate = new Item();
        countTemplate.setCategoryId(categoryId);
        int total = getCountByTemplate(countTemplate);
        int pageCount = calculatePageCount(total, perPage);

        if (pageNo >= pageCount) {
            pageNo = (pageCount > 0) ? pageCount - 1 : 0;
        } else if (pageCount < 0) {
            pageNo = 0;
        }

        int fromIndex = pageNo * perPage;
        int toIndex = fromIndex + perPage;

        SQLQuery<Item> itemSQLQuery = findPageByCategoryIdQuery.setParameter(1,fromIndex).setParameter(2, categoryId);
        log.info("fromIndex="+fromIndex + "; toIndex="+toIndex+"; toIndex-fromIndex="+(toIndex-fromIndex));
        log.info(itemSQLQuery.getQuery());
        return Arrays.asList(gigaSpace.readMultiple(itemSQLQuery,perPage));
    }

    public int getCountByTemplate(Item template) {
        return gigaSpace.count(template);
    }

    public int calculatePageCount(int total, int perPage) {
        return total / perPage + lastPageCorrection(total, perPage);
    }

    private int lastPageCorrection(long itemsTotal, long itemsPerPage) {
        return ((itemsTotal % itemsPerPage > 0) ? 1 : 0);
    }

    public void create(Item item) {
        item.setId(idGenerator.generateId());
        gigaSpace.write(item);
    }

    public void create(Item[] items) {
        for (Item item : items) {
            item.setId(idGenerator.generateId());
        }
        gigaSpace.writeMultiple(items);
    }

    public Item findById(Long id) {
        return gigaSpace.readById(Item.class, id);
    }

    public void update(Item item) {
        gigaSpace.write(item);
    }
}
