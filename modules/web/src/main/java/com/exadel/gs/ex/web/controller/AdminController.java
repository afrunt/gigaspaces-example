package com.exadel.gs.ex.web.controller;

import com.exadel.gs.ex.service.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    @Resource
    private CategoryService categoryService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String show(Model uiModel) {
        return "redirect:/admin/categories";
    }

}
