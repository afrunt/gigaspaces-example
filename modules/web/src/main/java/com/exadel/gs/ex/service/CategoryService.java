package com.exadel.gs.ex.service;

import com.exadel.gs.ex.model.Category;
import com.exadel.gs.ex.service.exception.AlreadyExistsException;
import com.exadel.gs.ex.service.exception.ServiceException;
import com.j_spaces.core.client.SQLQuery;
import org.openspaces.core.GigaSpace;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Service("categoryService")
public class CategoryService {
    @Resource
    private GigaSpace gigaSpace;

    private SQLQuery<Category> findAllQuery = new SQLQuery<Category>(Category.class, "ORDER BY name");

    public CategoryService() {
    }

    public Category findById(String id) {
        return gigaSpace.readById(Category.class, id);
    }

    public List<Category> findAll() {
        Category[] categories = gigaSpace.readMultiple(findAllQuery);
        return Arrays.asList(categories);
    }

    public void create(Category category) throws AlreadyExistsException {
        if(exists(category)) {
            throw new AlreadyExistsException("Category with ID=" + category.getId() + " already exists");
        }
        gigaSpace.write(category);
    }

    public void update(Category category) {
        gigaSpace.write(category);
    }

    public void delete(Category category) {
        gigaSpace.takeById(Category.class,category.getId());
    }

    public boolean exists(Category category) {
        if(category != null && category.getId() != null) {
            Category template = new Category();
            template.setId(category.getId());
            return gigaSpace.count(template) > 0;
        } else {
            return false;
        }
    }
}
