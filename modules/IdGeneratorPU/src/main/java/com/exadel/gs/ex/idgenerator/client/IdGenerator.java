package com.exadel.gs.ex.idgenerator.client;

import com.exadel.gs.ex.idgenerator.pu.IdCounterEntry;
import com.j_spaces.core.client.ReadModifiers;
import com.j_spaces.core.client.UpdateModifiers;
import net.jini.core.lease.Lease;
import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.context.GigaSpaceContext;
import org.openspaces.core.space.UrlSpaceConfigurer;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

public class IdGenerator {

    private long currentId = 0;

    @Resource
    GigaSpace gigaSpace;
    private long idLimit = -1;

    @Transactional(propagation = Propagation.REQUIRES_NEW, value = "transactionManager")
    public synchronized Long generateId() {
        if (currentId < 0 || currentId > idLimit) {
            getNextIdBatchFromSpace();
        }
        return currentId++;
    }

    private void getNextIdBatchFromSpace() {
        IdCounterEntry idCounterEntry = gigaSpace.readById(IdCounterEntry.class, 0L, 0L, 5000, ReadModifiers.EXCLUSIVE_READ_LOCK);
        if (idCounterEntry == null) {
            throw new RuntimeException("Could not get ID object from Space");
        }
        long[] range = idCounterEntry.getIdRange();
        currentId = range[0];
        idLimit = range[1];
        gigaSpace.write(idCounterEntry, Lease.FOREVER, 5000, UpdateModifiers.UPDATE_ONLY);
    }

    public void setGigaSpace(GigaSpace gigaSpace) {
        this.gigaSpace = gigaSpace;
    }
}
