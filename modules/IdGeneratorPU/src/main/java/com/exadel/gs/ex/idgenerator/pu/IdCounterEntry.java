package com.exadel.gs.ex.idgenerator.pu;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceExclude;
import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;

@SpaceClass
public class IdCounterEntry {

    private Long currentId;
    private Long idRangeSize;

    public IdCounterEntry() {}

    public IdCounterEntry(long currentId,long idRangeSize) {
        this.idRangeSize = idRangeSize;
        this.currentId = currentId; 
    }

    public Long getCurrentId() {
        return currentId;
    }

    public void setCurrentId(Long currentId) {
        this.currentId = currentId;
    }

    public Long getIdRangeSize() {
        return idRangeSize;
    }

    public void setIdRangeSize(Long idRangeSize) {
        this.idRangeSize = idRangeSize;
    }

    @SpaceExclude
    public long[] getIdRange() {
        long endId = currentId + idRangeSize;
        long[] range = new long[]{currentId, endId-1};
        currentId += idRangeSize;
        return range;
    }


    @SpaceId
    @SpaceRouting
    protected Long getRouting() {
        return 0L;
    }                                          
    protected void setRouting(Long routing) {}

}
