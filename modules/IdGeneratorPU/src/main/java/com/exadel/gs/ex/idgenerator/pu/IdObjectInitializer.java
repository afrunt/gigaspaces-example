package com.exadel.gs.ex.idgenerator.pu;

import com.j_spaces.core.client.UpdateModifiers;
import net.jini.core.lease.Lease;
import org.openspaces.core.GigaSpace;
import org.openspaces.core.cluster.ClusterInfo;
import org.openspaces.core.cluster.ClusterInfoContext;
import org.openspaces.core.context.GigaSpaceContext;
import org.springframework.beans.factory.InitializingBean;

public class IdObjectInitializer implements InitializingBean {

    @GigaSpaceContext
    private GigaSpace gigaSpace;

    @ClusterInfoContext
    private ClusterInfo clusterInfo;

    private long idRange = 1000;

    private long initialValue = 100;


    /**
     * Sets the range size,
     *
     */
    public void setIdRange(long idRange) {
        this.idRange = idRange;
    }

    public long getIdRange() {
        return idRange;
    }

    public void init() {
        System.out.println("IdObjectInitializer init called");
        if (shouldWriteIdObjectToSpace()) {
            IdCounterEntry existingEntry = gigaSpace.readById(IdCounterEntry.class, 0);
            if (existingEntry == null) {
                gigaSpace.write(new IdCounterEntry(initialValue, idRange), Lease.FOREVER, 0, UpdateModifiers.WRITE_ONLY);
                System.out.println("IdObjectInitializer wrote IdCounterEntry");
            }
        }
    }

    //return true if we don't have a clusterInfo/instance id, or if this the first
    //primary instance in the cluster
    private boolean shouldWriteIdObjectToSpace() {
        if (clusterInfo == null)
            return true;
        if (clusterInfo.getInstanceId() == null)
            return true;

        if (clusterInfo.getBackupId() != null)
            return false;

        if (clusterInfo.getInstanceId() == 1)
            return true;
        return false;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        init();
    }

    public long getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(long initialValue) {
        this.initialValue = initialValue;
    }
}
